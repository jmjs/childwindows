let cw = window.open("http://0.0.0.0:8000/child.html", '*', "menubar=no")

let countId = document.getElementById("count");
let totalCount = 0

function sendChild(data){
    cw.parentMessage(data);
}


document.getElementById("add").addEventListener("click", ()=>{
    totalCount = countId.innerText
    countId.innerText = ""
    countId.innerText = ++totalCount
    sendChild(totalCount)
})

document.getElementById("remove").addEventListener("click", ()=>{
    totalCount = countId.innerText
    countId.innerText = ""
    countId.innerText = --totalCount
    sendChild(totalCount)
})

function receiveChildMessage(data){
    console.log(data)
}