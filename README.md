# childWindows

objectif : ouvrir une fenetre enfant, et faire transiter des données entre les deux

## structure

 - le couple index.html et index.js est le parent  
- le couple child.html et child.js est l'enfant

## fonctionnement

### parent
```js
let cw = window.open("http://0.0.0.0:8000/child.html", '*', "menubar=no")
```
permet de lancer la fenetre enfant
L'objet window de la feentre enfant est stocké dans cw


```js
function sendChild(data){
    cw.parentMessage(data);
}
```
cette fonction appel la méthode parentMessage depuis l'objet window de la fenêtre enfant. Elle prend en argument les données à faire transiter

### enfant
```js
function parentMessage(data){
 //stuff
}
```

cette fonction est appelé depuis le parent et récupère en paramètre les données du parent